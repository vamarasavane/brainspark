# The BrainSpark Project

This project contains snippets of Scala code for illustrating various
Apache Spark concepts. It is
intended to help you _get started_ with learning Apache Spark (as a _Scala_ programmer) by providing a super easy on-ramp that _doesn't_ involve Unix, cluster configuration, building from sources or
installing Hadoop. Many of these activities will be necessary later in your
learning experience, after you've used these examples to achieve basic familiarity.



## Dependencies

The project was created with IntelliJ Idea 14 Community Edition,
JDK 1.7, Scala 2.11.2 and Spark 1.6.0 on Mac OSX.

Versions of these examples for other configurations (older versions of Scala and Spark) can be found in various branches.

## Examples

The examples can be found under src/main/scala. The best way to use them is to start by reading the code and its comments. Then, since each file contains an object definition with a main method, run it and consider the output. Relevant blog posts and StackOverflow answers are listed below. 

| Package | File                  | What's Illustrated    |
|---------|-----------------------|-----------------------|
|         | P1_HelloRDD         | How to execute your first, very simple, Spark Job.
|         | P2_Compute      | How RDDs work in more complex computations. |
|         | P3_Combining     | Operations on multiple RDDs |
|         | P4_MoreOperation | More complex operations on individual RDDs |
|         | P5_Partitions        | Explicit control of partitioning for performance and scalability. |
|         | P6_Accumulators | How to use Spark accumulators to efficiently gather the results of distributed computations. |
| [hiveql](src/main/scala/hiveql)  |  | Using HiveQL features in a HiveContext. See the local README.md in that directory for details. |
| special | CustomPartitioner | How to take control of the partitioning of an RDD. |
| special | HashJoin         | How to use the well known Hash Join algorithm to join two RDDs where one is small enough to entirely fit in the memory of each partition.|
| special | PairRDD | How to operate on RDDs in which the underlying elements are pairs. |
| [dataframe](src/main/scala/dataframe) |  | A range of DataFrame examples -- see the local README.md in that directory for details. |
| experiments | | Experimental examples that may or may not lead to anything interesting. |
| [sql](src/main/scala/sql) | | A range of SQL examples -- see the local README.md in that directory for details.  |
| streaming | FileBased | Streaming from a sequence of files. |
| streaming | QueueBased | Streaming from a queue. |
| streaming | Accumulation | Accumulating stream data in a single RDD. |
| streaming | Windowing | Maintaining a sliding window on the most recent stream data. |
| [graphx](src/main/scala/graphx) | | A range of GraphX examples -- see the local README.md in that directory for details. |





Running Exemple : P1_HelloRDD
/Users/vamarasavane/spark-1.6.0-bin-hadoop2.4/bin/spark-submit --class "P1_HelloRDD" --master local[4] /Volumes/Storage/BrainSpark/target/scala-2.11/brainspark_2.11-1.0.jar

 
Running Exemple : P2_Compute
/Users/vamarasavane/spark-1.6.0-bin-hadoop2.4/bin/spark-submit --class "P2_Compute" --master local[4] /Volumes/Storage/BrainSpark/target/scala-2.11/brainspark_2.11-1.0.jar

Running Exemple : P3_Combining
/Users/vamarasavane/spark-1.6.0-bin-hadoop2.4/bin/spark-submit --class "P3_Combining" --master local[4] /Volumes/Storage/BrainSpark/target/scala-2.11/brainspark_2.11-1.0.jar


