import org.apache.spark.{SparkContext, SparkConf}

/**
  * Created by vamarasavane on 25/04/2016.
  */
object P1_HelloRDD {

  def main(args: Array[String]): Unit ={

    val conf = new SparkConf()
    conf.set("spark.app.name","P1_HelloRDD")
    //conf.set("spark.driver.core","1")
    //conf.set("spark.driver.maxResultSize","1g")
    //conf.set("spark.driver.memory","512m")
    //conf.set("spark.executor.memory","512m")
    conf.set("spark.master","local[4]")
    val sc = new SparkContext(conf)


    // put some data in an RDD
    val rangeNumbers = 1 to 10
    val rangeNumbersRDD = sc.parallelize(rangeNumbers, 4)
    println("Print each element of the rangeNumbersRDD")
    rangeNumbersRDD.foreach(println)

    //operate on the numbers
    val stillAnRDD = rangeNumbersRDD.map(n => n.toDouble / 10)

    //the data back out
    val nowAnArray = stillAnRDD.collect()
    // interesting how the array comes out sorted but the RDD didn't
    println("Now print each element of the transformed array")
    nowAnArray.foreach(println)

    // explore RDD properties
    val partitions = stillAnRDD.glom()
    println("We _should_ have 4 partitions")
    println(partitions.count())
    partitions.foreach(a => {
      println("Partition contents:" +
        a.foldLeft("")((s, e) => s + " " + e))
    })


  }

}
