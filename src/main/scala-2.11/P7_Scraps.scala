import org.apache.spark.{SparkContext, SparkConf}

/**
  * Created by vamarasavane on 25/04/2016.
  */
object P7_Scraps {

  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("P7_Scraps").setMaster("local[4]")
    val sc = new SparkContext(conf)

    // look at the distribution of numbers across partitions
    val numbers =  sc.parallelize(1 to 100, 4)


    // preferredLocations -- not too interesting right now
    numbers.partitions.foreach(p => {
      println("Partition: " + p.index)
      numbers.preferredLocations(p).foreach(s => println("  Location: " + s))
    })

    // TODO: Partitioner

    // TODO: PairRDDFunctions

  }


}
